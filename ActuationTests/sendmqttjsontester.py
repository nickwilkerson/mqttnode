#!/usr/bin/python
import json
import mosquitto
import os
import time
import RPi.GPIO as GPIO

broker = "127.0.0.1"
port = 1883
nodename = 'raspberrypi1'

PIR = 14


redState = False
greenState = False
blueState = False

def on_connect(obj, rc):
    if rc == 0:
        print("Connected successfully.")

def on_disconnect(obj, rc):
    print("Disconnected successfully.")

def on_publish(obj, mid):
    print("Message "+str(mid)+" published.")

def on_message(obj, msg):
	print("Message received on topic "+msg.topic+" with QoS "+str(msg.qos)+" and payload "+msg.payload)
	
def on_subscribe(obj, mid, qos_list):
    print("Subscribe with mid "+str(mid)+" received.")

def on_unsubscribe(obj, mid):
    print("Unsubscribe with mid "+str(mid)+" received.")


mypid = os.getpid()
client_uniq = "pubclient_"+str(mypid)
client = mosquitto.Mosquitto(client_uniq)
client.on_connect = on_connect
client.on_disconnect = on_disconnect
client.on_publish = on_publish
client.on_message = on_message
client.on_subscribe = on_subscribe
client.on_unsubscribe = on_unsubscribe
client.connect(broker, port, 60, True)
client.subscribe(nodename + '/command',0)


actuator1 = {'id':'red','value':redState}
actuator2 = {'id':'green','value':greenState}
actuator3 = {'id':'blue','value':blueState}
actuators = [actuator1, actuator2, actuator3]
root = {'type':'command','from':'test','to':'raspberrypi1','actuators':actuators}
output = json.dumps(root)
print output
client.publish("raspberrypi1/command",output)

