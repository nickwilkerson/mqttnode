/*
*
*  MQTTclient, a MQTT (http://mqtt.com) client for Arduino Yun
*  Copyright 2013 (c) Martin Dal
*  Bobby Technologies I/S (http://bobbytechnologies.dk)
*
*  OpenWRT mosquitto client needed. Read the readme if in doubt! 
*  
*/

// include process library
#include <Process.h>
#include <aJSON.h>
#include "MQTTclient.h"

#define MQTT_HOST "nickwilkerson.no-ip.org" // test.mosquitto.org

char led = 13;

long statusDelay = 100;
long configDelay = 1000;
long statusTime;
long configTime;


void setup() {
    
  // start serial
  Serial.begin(9600);
  Serial.print("hello\n");
  // remember the bridge!
  Bridge.begin();
  // begin the client library (initialize host)
  mqtt.begin(MQTT_HOST, 1883);
  
  pinMode(led, OUTPUT);
  // make some subscriptions
  mqtt.subscribe("arduinoyun1/command", commandHandler);
  
  configTime = millis() + configDelay;
  statusTime = millis() + statusDelay;

}

void loop() {
//  Serial.print("millis()=");
  unsigned long time = millis();
//  Serial.print(time);
//  Serial.print("\n");
//  Serial.print("configTime=");
//  Serial.print(configTime);
//  Serial.print("\n");
  if (time > configTime) {
    Serial.println("configTime");
    aJsonObject *root, *sensors, *sensor1, *sensor2, *sensor3;
    //configuration packet
    root=aJson.createObject();
    aJson.addStringToObject(root, "type", "status");
    aJson.addStringToObject(root, "name", "arduino1");
    aJson.addItemToObject(root, "sensors", sensors = aJson.createArray());
    sensor1 = aJson.createObject();
    aJson.addStringToObject(sensor1, "id", "1");
    aJson.addStringToObject(sensor1, "type", "bool");
    aJson.addStringToObject(sensor1, "value", "1");
    aJson.addItemToArray(sensors, sensor1);
    sensor2 = aJson.createObject();
    aJson.addStringToObject(sensor2, "id", "2");
    aJson.addStringToObject(sensor2, "type", "bool");
    aJson.addStringToObject(sensor2, "value", "1");
    aJson.addItemToArray(sensors, sensor2);
    sensor3 = aJson.createObject();
    aJson.addStringToObject(sensor3, "id", "3");
    aJson.addStringToObject(sensor3, "type", "bool");
    aJson.addStringToObject(sensor3, "value", "1");
    aJson.addItemToArray(sensors, sensor3);
    char *string = aJson.print(root);
    aJson.deleteItem(root);
    Serial.print(string);
    mqtt.publish("arduinoyun1/configuration", string);
    free(string);
    if (time > (unsigned long)(4294967295 - configDelay)) {
      configTime = configDelay;
    } else {
      configTime = time + configDelay;
    }
      
  } else if (time > statusTime) {
    //status packet
    aJsonObject *root, *sensors, *sensor1, *sensor2, *sensor3;
    root=aJson.createObject();
    aJson.addStringToObject(root, "type", "status");
    aJson.addStringToObject(root, "name", "arduino1");
    aJson.addItemToObject(root, "sensors", sensors = aJson.createArray());
    sensor1 = aJson.createObject();
    aJson.addStringToObject(sensor1, "id", "1");
    aJson.addStringToObject(sensor1, "type", "bool");
    aJson.addStringToObject(sensor1, "value", "1");
    aJson.addItemToArray(sensors, sensor1);
    sensor2 = aJson.createObject();
    aJson.addStringToObject(sensor2, "id", "2");
    aJson.addStringToObject(sensor2, "type", "bool");
    aJson.addStringToObject(sensor2, "value", "1");
    aJson.addItemToArray(sensors, sensor2);
    sensor3 = aJson.createObject();
    aJson.addStringToObject(sensor3, "id", "3");
    aJson.addStringToObject(sensor3, "type", "bool");
    aJson.addStringToObject(sensor3, "value", "1");
    aJson.addItemToArray(sensors, sensor3);
    char *string = aJson.print(root);
    aJson.deleteItem(root);
    Serial.print(string);
    mqtt.publish("arduinoyun1/status", string);
    free(string);
    if (time > (unsigned long)(4294967295 - statusDelay)) {
      statusTime = statusDelay;
    } else {
      statusTime = time + statusDelay;
    }
  }
 // Serial.print("hello");
  // check for incoming events
  mqtt.monitor();
// Serial.print("hello");
}

// use callback function to work with your messages
void commandHandler(const String& topic, const String& subtopic, const String& message) {
  
  // print the topic and message
  Serial.print("topic: ");
  Serial.println(topic);
  Serial.print("message: "); 
  Serial.println(message); 
  
  int integer = 25;
  
  // publish an integer to a test topic
  mqtt.publish("test/mqttclient/publish1", integer);
  
}



