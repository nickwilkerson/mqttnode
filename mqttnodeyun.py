#!/usr/bin/python
import json
import mosquitto
import os
import time
import RPi.GPIO as GPIO

INPUT = True	#Don't change this
OUTPUT = False	#Don't change this
ON = True		#Don't change this
OFF = False		#Don't chnage this
#Configure this part
broker = "127.0.0.1"
port = 1883
nodename = 'yun1'
#to Here

pinStates = {'pin14':INPUT,'pin15':INPUT,'pin18':INPUT,'pin23':INPUT,'pin24':INPUT,'pin25':INPUT,'pin8':INPUT,'pin7':INPUT,'pin0':INPUT,'pin1':INPUT,'pin4':INPUT,'pin17':OUTPUT,'pin21':OUTPUT,'pin22':OUTPUT,'pin10':INPUT,'pin9':INPUT,'pin11':INPUT}
pinValues = {'pin14':OFF,'pin15':OFF,'pin18':OFF,'pin23':OFF,'pin24':OFF,'pin25':OFF,'pin8':OFF,'pin7':OFF,'pin0':OFF,'pin1':OFF,'pin4':OFF,'pin17':OFF,'pin21':OFF,'pin22':OFF,'pin10':OFF,'pin9':OFF,'pin11':OFF}
pinNames = {'pin14':'','pin15':'','pin18':'','pin23':'','pin24':'','pin25':'','pin8':'','pin7':'','pin0':'','pin1':'','pin4':'','pin17':'','pin21':'','pin22':'','pin10':'','pin9':'','pin11':''}
pinNumbers = {'pin14':14,'pin15':15,'pin18':18,'pin23':23,'pin24':24,'pin25':25,'pin8':8,'pin7':7,'pin0':0,'pin1':1,'pin4':4,'pin17':17,'pin21':21,'pin22':22,'pin10':10,'pin9':9,'pin11':11}

def on_connect(obj, rc):
    if rc == 0:
        print("Connected successfully.")

def on_disconnect(obj, rc):
    print("Disconnected successfully.")

def on_publish(obj, mid):
    print("Message "+str(mid)+" published.")

def on_message(obj, msg):
	print("Message received on topic "+msg.topic+" with QoS "+str(msg.qos)+" and payload "+msg.payload)
	if msg.topic == nodename + '/command':
		print("recieved command")
		root = json.loads(msg.payload)
		if root['type'] == 'configure' and root['to'] == nodename:
			pins = root['pins']
			for pin in pins:
				if 'pin' + pin['number'] in pinStates:
					if 'state' in pin:
						if pin['state'] == 'input':
							pinStates['pin' + pin['number']] = INPUT
							GPIO.setup(pinNumbers['pin' + pin['number']],GPIO.IN)
						elif pin['state'] == 'output':
							pinStates['pin' + pin['number']] = OUTPUT
							GPIO.setup(pinNumbers['pin' + pin['number']],GPIO.OUT)
						else:
							print('state:' + pin['state'] + ' not recognized')
					if 'name' in pin:
						pinNames['pin' + pin['number']] = pin['name']
				else:
					print('pin' + pin['number not recognized'])
		elif root['type'] == 'command' and root['to'] == nodename:
			print("	Found a command packet")
			actuators = root['actuators']
			for actuator in actuators:
				if 'pin' + actuator['number'] in pinStates:
					if pinStates['pin' + actuator['number']] == OUTPUT:
						if actuator['value'] == '1':
							pinValues['pin' + actuator['number']] = ON
							GPIO.output(pinNumbers['pin' + actuator['number']], GPIO.HIGH)
						elif actuator['value'] == '0':
							pinValues['pin' + actuator['number']] = OFF
							GPIO.output(pinNumbers['pin' + actuator['number']], GPIO.LOW)
						else:
							print('unrecognized value: ' + actuator['value'] + ' for pin' + actuator['number'])
					else:
						print('can\'t actuate pin' + actuator['number'] + ' because it is an input')
				else:
					print('unrecognized actuator: ' + actuator['number'])

def on_subscribe(obj, mid, qos_list):
    print("Subscribe with mid "+str(mid)+" received.")

def on_unsubscribe(obj, mid):
    print("Unsubscribe with mid "+str(mid)+" received.")


mypid = os.getpid()
client_uniq = "pubclient_"+str(mypid)
client = mosquitto.Mosquitto(client_uniq)
client.on_connect = on_connect
client.on_disconnect = on_disconnect
client.on_publish = on_publish
client.on_message = on_message
client.on_subscribe = on_subscribe
client.on_unsubscribe = on_unsubscribe
client.connect(broker, port, 60, True)
client.subscribe(nodename + '/command',0)

GPIO.setmode(GPIO.BCM)
for pin in pinStates:
	if pinStates[pin] == INPUT:#These can be changed by a message
		GPIO.setup(pinNumbers[pin],GPIO.IN)
	elif pinStates[pin] == OUTPUT:
		GPIO.setup(pinNumbers[pin],GPIO.OUT)


#GPIO.setup(pinNumbers['pin14'],GPIO.IN)	#These can be changed by a message
#GPIO.setup(pinNumbers['pin15'],GPIO.IN)
#GPIO.setup(pinNumbers['pin18'],GPIO.IN)
#GPIO.setup(pinNumbers['pin23'],GPIO.IN)
#GPIO.setup(pinNumbers['pin24'],GPIO.IN)
#GPIO.setup(pinNumbers['pin25'],GPIO.IN)
#GPIO.setup(pinNumbers['pin8'],GPIO.IN)
#GPIO.setup(pinNumbers['pin7'],GPIO.IN)
#GPIO.setup(pinNumbers['pin0'],GPIO.IN)
#GPIO.setup(pinNumbers['pin1'],GPIO.IN)
#GPIO.setup(pinNumbers['pin4'],GPIO.IN)
#GPIO.setup(pinNumbers['pin17'],GPIO.OUT)
#GPIO.setup(pinNumbers['pin21'],GPIO.OUT)
#GPIO.setup(pinNumbers['pin22'],GPIO.OUT)
#GPIO.setup(pinNumbers['pin10'],GPIO.IN)
#GPIO.setup(pinNumbers['pin9'],GPIO.IN)
#GPIO.setup(pinNumbers['pin11'],GPIO.IN)

#GPIO.setup(PIR,GPIO.IN)
#GPIO.setup(RED,GPIO.OUT)
#GPIO.setup(GREEN,GPIO.OUT)
#GPIO.setup(BLUE,GPIO.OUT)

configurationCounter = 0;
while client.loop()==0:
	configurationCounter = configurationCounter + 1
	sensors = []
	actuators = []
	for pin in pinStates:
		if pinStates[pin] == INPUT:
			sensorValue = GPIO.input(pinNumbers[pin])
			pinValues[pin] = sensorValue
			sensor = {'number':str(pinNumbers[pin]),'value':str(pinValues[pin])}
			sensors.append(sensor)
		elif pinStates[pin] == OUTPUT:
			if pinValues[pin] == False:
				actuator = {'number':str(pinNumbers[pin]),'value':'0'}
			elif pinValues[pin] == True:
				actuator = {'number':str(pinNumbers[pin]),'value':'1'}
			actuators.append(actuator)
		else:
			print('unrecognized state found for ' + pin)

	statusRoot = {'type':'status','from':nodename,'sensors':sensors,'actuators':actuators}
	statusOutput = json.dumps(statusRoot)
	print statusOutput
	client.publish(nodename + "/status",statusOutput)
	if configurationCounter == 10:
		pins = []
		sensors = []
        	actuators = []
		for pin in pinStates:
			if pinStates[pin] == INPUT:
				pinTypeString = 'input'
			        sensorValue = GPIO.input(pinNumbers[pin])
                        	pinValues[pin] = sensorValue
                        	sensor = {'number':str(pinNumbers[pin]),'value':str(pinValues[pin])}
                        	sensors.append(sensor)
			elif pinStates[pin] == OUTPUT:
				pinTypeString = 'output'
				if pinValues[pin] == False:
                                	actuator = {'number':str(pinNumbers[pin]),'value':'0'}
                        	elif pinValues[pin] == True:
                                	actuator = {'number':str(pinNumbers[pin]),'value':'1'}
                        	actuators.append(actuator)

			else:
				print('unrecognized state found for ' + pin)
			pinInfo = {'number':str(pinNumbers[pin]),'state':pinTypeString,'name':pinNames[pin]}
			pins.append(pinInfo)
		configurationRoot = {'type':'configuration','from':nodename,'pins':pins,'sensors':sensors,'actuators':actuators}
		configurationOutput = json.dumps(configurationRoot)
		print configurationOutput
		client.publish(nodename + "/configuration",configurationOutput)
		configurationCounter = 0
	time.sleep(1)
	pass
